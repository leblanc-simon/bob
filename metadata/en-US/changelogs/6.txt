Version 1.3.0
- Added note deletion. Thanks @Simon Leblanc
- Added button to choose term calculation based on 38 or 39 weeks of pregnancy.
- Added language selection.
- Fix issue with displaying the right fruit on the fruit page.
- Italian version of the application. Thanks @AlexKalopsia

Version 1.2.2
- Build fix

Version 1.2.1
- Fix of the changelog of the previous version

Version 1.2.0
- German application translation
- Light/dark theme selector
- Android 14 and Material Design 3 version 1.2 support for Compose
- Various fixes