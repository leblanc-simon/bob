package com.baldo.bob.ui.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory.Companion.APPLICATION_KEY
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.baldo.bob.dataStore.AppSettings
import com.baldo.bob.dataStore.UserInformations
import com.baldo.bob.dataStore.UserInformationsRepository
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

class BobViewModel(
    private val userInformationsRepository: UserInformationsRepository,
) : ViewModel() {

    val uiState: StateFlow<BobUiState> =
        userInformationsRepository.userPreferencesFlow.map { data ->
            BobUiState(
                userName = data.userName,
                userLastPeriodsDate = data.lastPeriodDate,
                userLastOvulationDate = data.lastOvulationDate,
                numberOfWeekForCalculationTerm = data.numberOfWeekForCalculationTerm

            )
        }.stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5_000),
            initialValue = BobUiState()
        )

    val appUIState: StateFlow<AppUiState> =
        userInformationsRepository.appSettingsPreferencesFlow.map { data ->
            AppUiState(
                themeSetting = data.themeSetting
            )
        }.stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5_000),
            initialValue = AppUiState()
        )

    val userLanguageUiState: StateFlow<UserLanguageUiState> =
        userInformationsRepository.userLanguagePreferencesFlow.map { data ->
            UserLanguageUiState(
                userLanguage = data.userLanguage
            )
        }.stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5_000),
            initialValue = UserLanguageUiState()
        )

    fun updateUserInformations(userInformations: UserInformations) {
        viewModelScope.launch {
            userInformationsRepository.saveUserInformations(userInformations)
        }
    }

    fun updateAppSettings(appSettings: AppSettings) {
        viewModelScope.launch {
            userInformationsRepository.saveAppSettings(appSettings)
        }
    }

    fun updateUserLanguage(userLanguage: Int) {
        viewModelScope.launch {
            userInformationsRepository.setUserLanguage(userLanguage)
        }
    }

    companion object {
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                val application = (this[APPLICATION_KEY] as com.baldo.bob.BobApplication)
                BobViewModel(application.userInformationsRepository)
            }
        }
    }
}